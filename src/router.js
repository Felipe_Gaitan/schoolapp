import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Instituciones from './components/Instituciones.vue';
import Estudiantes from './components/Estudiantes.vue';
import Usuarios from './components/Usuarios.vue';
import Principal from './components/Principal.vue';
import Profesores from './components/Profesores.vue';
import Menu from './components/Menu.vue';
import RegistrarAsistencia from './components/RegistrarAsistencia.vue';
import RegistrarAsistenciaAdm from './components/RegistrarAsistenciaAdm.vue';
import NotificarAsistencia from './components/NotificarAsistencia.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'principal',
      component: Principal,
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
    },

    {
      path: '/menu',
      name: 'menu',
      component: Menu,
    },

    {
      path: '/notificarAsistencia',
      name: 'notificarAsistencia',
      component: NotificarAsistencia,
    },

    {
      path: '/registrarAsistencia',
      name: 'registrarAsistencia',
      component: RegistrarAsistencia,
    },

    {
      path: '/registrarAsistenciaAdm',
      name: 'registrarAsistenciaAdm',
      component: RegistrarAsistenciaAdm,
    },

    {
      path: '/instituciones',
      name: 'instituciones',
      component: Instituciones,
    },
    {
      path: '/estudiantes',
      name: 'estudiantes',
      component: Estudiantes,
    },
    {
      path: '/profesores',
      name: 'profesores',
      component: Profesores,
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuarios,
    },
    // {
    //   path: '/principal',
    //   name: 'principal',
    //   component: Principal,
    // },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
