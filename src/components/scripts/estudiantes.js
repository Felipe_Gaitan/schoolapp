import axios from 'axios';
import Alert from '../Alert.vue';

export default {
  data() {
    return {
      institucion: '',
      grados: ['', 'Primero','Segundo','Tercero','Cuarto','Quinto','Sexto','Séptimo','Octavo','Noveno','Décimo','Once'],
      grupos: ['', 'A', 'B', 'C'],
      grupoSeleccionado: {grado: '', grupo: ''},   
      estudiantes: [],
      formCrearEst: {
        documento: '',
        nombre: '',
        apellidos: '',
        telefono:'',
        direccion:'',
        correo: '',
      },
      mensaje: '',
      mostrarMsj: false,
      formEditEst: {
        documento: '',
        nombre: '',
        apellidos: '',
        telefono:'',
        direccion:'',
        correo: '',
      },
    };
  },
  components: {
    alert: Alert,
  },   
  methods: {
    todoEstudiantes() {
      const path = 'http://localhost:5000/estudiantes';
      axios.get(path)
        .then((res) => {
          this.estudiantes = res.data['estudiantes'];
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarEstudiante(payload) {
      const path = 'http://localhost:5000/estudiantes';
      axios.post(path, payload)
        .then(() => {
          this.todoEstudiantes();
          this.mensaje = 'Estudiante añadido!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.todoEstudiantes();
        });
    },
    editEst(estudiante) {
      this.formEditEst = estudiante;
    },
    actualizarEstudiante(payload, idEstudiante) {
      const path = `http://localhost:5000/estudiantes/${idEstudiante}`;
      axios.put(path, payload)
        .then(() => {
          this.todoEstudiantes();
          this.mensaje = 'Estudiante actualizado!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoEstudiantes();
        });
    },
    onSubmitUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditEst.hide();
      const payload = {
        documento: this.formEditEst.documento,
        nombre: this.formEditEst.nombre,
        apellidos: this.formEditEst.apellidos,
        telefono: this.formEditEst.telefono,
        direccion: this.formEditEst.direccion,
        correo: this.formEditEst.correo,
        institucion_id: this.institucion,
        grado: this.grupoSeleccionado.grado,
        grupo: this.grupoSeleccionado.grupo
      };
      this.actualizarEstudiante(payload, this.formEditEst._id);
    },
    activate() {
      setTimeout(() => this.mostrarMsj = false, 1500);
    },
    initForm() {
      this.formCrearEst.documento = '';
      this.formCrearEst.nombre = '';
      this.formCrearEst.apellidos = '';
      this.formCrearEst.telefono = '';
      this.formCrearEst.direccion = '';
      this.formCrearEst.correo = '';
      this.formEditEst.documento = '';
      this.formEditEst.nombre = '';
      this.formEditEst.apellidos = '';
      this.formEditEst.telefono = '';
      this.formEditEst.direccion = '';
      this.formEditEst.correo = '';
    },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearEst.hide();
      const payload = {
        documento: this.formCrearEst.documento,
        nombre: this.formCrearEst.nombre,
        apellidos: this.formCrearEst.apellidos,
        telefono: this.formCrearEst.telefono,
        direccion: this.formCrearEst.direccion,
        correo: this.formCrearEst.correo,
        institucion_id: this.institucion,
        grado: this.grupoSeleccionado.grado,
        grupo: this.grupoSeleccionado.grupo
      };
      this.agregarEstudiante(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearEst.hide();
      this.initForm();
    },
    onResetUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditEst.hide();
      this.initForm();
      this.todoEstudiantes(); // why?
    },
    eliminarEstudiante(idEstudiante) {
      const path = `http://localhost:5000/estudiantes/${idEstudiante}`;
      axios.delete(path)
        .then(() => {
          this.todoEstudiantes();
          this.mensaje = 'Estudiante eliminado!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoEstudiantes();
        });
    },
    OnEliminarEst(estudiante) {
      this.eliminarEstudiante(estudiante._id);
    },
  },
  created() {
    this.todoEstudiantes();
  },
};