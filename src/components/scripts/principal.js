import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import style from './style.scss';
  

  
export default {
  data(middleSize: ['hidden-all','lg','xl']) {
    return {
     principal: [],
      formCrearPrincipal: {
        contacto: '',
       
      },
    };
  },
  methods: {
    getPrincipal() {
      const path = 'http://localhost:5000/principal';
      axios.get(path)
        .then((res) => {
          this.principal = res.data;
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarPrincipal(payload) {
      const path = 'http://localhost:5000/principal';
      axios.post(path, payload)
        .then(() => {
          this.getPrincipal();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.getPrincipal();
        });
    },
    initForm() {
      this.formCrearPrincipal.contacto = '';
        },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearPrincipal.hide();
      let read = false;
      if (this.formCrearPrincipal.read[0]) read = true;
      const payload = {
        title: this.formCrearPrincipal.contacto,
    
        
      };
      this.agregarPrincipal(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearPrincipal.hide();
      this.initForm();
    },
  },
  created() {
    this.getPrincipal();
  },
};