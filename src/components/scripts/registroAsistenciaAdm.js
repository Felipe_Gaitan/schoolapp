import axios from 'axios';

export default {
  data() {
    return {
      estudiante: [],
      formCrearEstudiante: {
        Nombre: '',
        Apellidos: '',
        Excusa: '',        
      },
    };
  },
  methods: {
    getProfesores() {
      const path = 'http://localhost:5000/profesores';
      axios.get(path)
        .then((res) => {
          this.profesores = res.data;
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarProfesor(payload) {
      const path = 'http://localhost:5000/profesores';
      axios.post(path, payload)
        .then(() => {
          this.getProfesores();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.getProfesores();
        });
    },
    initForm() {
      this.formCrearProfesor.Documento = '';
      this.formCrearProfesor.Nombre = '';
      this.formCrearProfesor.Apellidos = '';
      this.formCrearProfesor.Correo = '';
      this.formCrearProfesor.Telefono = '';
      this.formCrearProfesor.Direccion = [];
      this.formCrearProfesor.DirectorGrupo = '';
      this.formCrearProfesor.Materias = [];
    },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearProfesor.hide();
      let read = false;
      if (this.formCrearProfesor.read[0]) read = true;
      const payload = { //OJO ACÁ, PREGUNTAR COMO FUNCIONA ESTO
        title: this.formCrearProfesor.Nombre,
        author: this.formCrearProfesor.telefono,
        author: this.formCrearInst.direccion,
      };
      this.agregarProfesor(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearProfesor.hide();
      this.initForm();
    },
  },
  created() {
    this.getProfesores();
  },
};