import axios from 'axios';
import Alert from '../Alert.vue';

export default {
  data() {
    return {
      profesores: [],
      formCrearProf: {
        Documento: '',
        Nombre: '',
        Apellidos: '',
        Correo: '',
        Telefono: '',
        Direccion: '',
        DirectorGrupo: '',
        Materias: '',
      },
      mensaje: '',
      mostrarMsj: false,
      formEditProf: {
        id: '',
        Documento: '',
        Nombre: '',
        Apellidos: '',
        Correo: '',
        Telefono: '',
        Direccion: '',
        DirectorGrupo: '',
        Materias: '',
         },
    };
  },
  components: {
    alert: Alert,
  },
  methods: {
    todoProfesores() {
      const path = 'http://localhost:5000/profesores';
      axios.get(path)
        .then((res) => {
          this.profesores = res.data['profesores'];
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarProfesor(payload) {
      const path = 'http://localhost:5000/profesores';
      axios.post(path, payload)
        .then(() => {
          this.todoProfesores();
          this.mensaje = 'Profesor añadido!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.todoProfesores();
        });
    },
    editProf(profesor) {
      this.formEditProf = profesor;
    },
    actualizarProfesor(payload, idProfesor) {
      const path = `http://localhost:5000/profesores/${idProfesor}`;
      axios.put(path, payload)
        .then(() => {
          this.todoProfesores();
          this.mensaje = 'Profesor actualizado!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoProfesores();
        });
    },
    onSubmitUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditProf.hide();
      const payload = {
        _id: this.formEditProf._id,
        Documento: this.formEditProf.Documento,
        Nombre: this.formEditProf.Nombre,
        Apellidos: this.formEditProf.Apellidos,
        Correo: this.formEditProf.Correo,
        Telefono: this.formEditProf.Telefono,
        Direccion: this.formEditProf.Direccion,
        DirectorGrupo: this.formEditProf.DirectorGrupo,
        Materias: this.formEditProf.Materias,
      };
      this.actualizarProfesor(payload, this.formEditProf._id);
    },
    activate() {
      setTimeout(() => this.mostrarMsj = false, 1500);
    },
    initForm() {
      this.formCrearProf.Documento = '';
      this.formCrearProf.Nombre = '';
      this.formCrearProf.Apellidos = '';
      this.formCrearProf.Correo = '';
      this.formCrearProf.Telefono = '';
      this.formCrearProf.Direccion = '';
      this.formCrearProf.DirectorGrupo = '';
      this.formCrearProf.Materias = '';
      this.formEditProf._id = '';
      this.formEditProf.Documento = '';
      this.formEditProf.Nombre = '';
      this.formEditProf.Apellidos = '';
      this.formEditProf.Correo = '';
      this.formEditProf.Telefono = '';
      this.formEditProf.Direccion = '';
      this.formEditProf.DirectorGrupo = '';
      this.formEditProf.Materias = '';
    },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearProf.hide();
      const payload = {
        Documento: this.formCrearProf.Documento,
        Nombre: this.formCrearProf.Nombre,
        Apellidos: this.formCrearProf.Apellidos,
        Correo: this.formCrearProf.Correo,
        Telefono: this.formCrearProf.Telefono,
        Direccion: this.formCrearProf.Direccion,
        DirectorGrupo: this.formCrearProf.DirectorGrupo,
        Materias: this.formCrearProf.Materias,
      };
      this.agregarProfesor(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearProf.hide();
      this.initForm();
    },
    onResetUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditProf.hide();
      this.initForm();
      this.todoProfesores(); // why?
    },
    eliminarProfesor(idProfesor) {
      const path = `http://localhost:5000/profesores/${idProfesor}`;
      axios.delete(path)
        .then(() => {
          this.todoProfesores();
          this.mensaje = 'Profesor eliminado!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoProfesores();
        });
    },
    OnEliminarProf(profesor) {
      this.eliminarProfesor(profesor._id);
    },
  },
  created() {
    this.todoProfesores();
  },
};
        
