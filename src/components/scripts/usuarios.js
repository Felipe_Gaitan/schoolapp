import axios from 'axios';

export default {
  data() {
    return {
      usuarios: [],
      formCrearUsuarios: {
        usuario: '',
        contraseña: [],
      },
    };
  },
  methods: {
    getUsuarios() {
      const path = 'http://localhost:5000/usuarios';
      axios.get(path)
        .then((res) => {
          this.usuarios = res.data;
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarUsuarios(payload) {
      const path = 'http://localhost:5000/usuarios';
      axios.post(path, payload)
        .then(() => {
          this.getUsuarios();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.getUsuarios();
        });
    },
    initForm() {
      this.formCrearUsuarios.usuario = '';
      this.formCrearUsuarios.contraseña = [];
    },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearUsuarios.hide();
      let read = false;
      if (this.formCrearUsuarios.read[0]) read = true;
      const payload = {
        title: this.formCrearUsuarios.usuario,
        author: this.formCrearUsuarios.contraseña,
        
      };
      this.agregarUsuarios(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearUsuarios.hide();
      this.initForm();
    },
  },
  created() {
    this.getUsuarios();
  },
};