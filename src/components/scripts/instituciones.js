import axios from 'axios';
import Alert from '../Alert.vue';

export default {
  data() {
    return {
      instituciones: [],
      formCrearInst: {
        nombre: '',
        correo: '',
        direccion: '',
        telefono: '',
        ciudad: '',
      },
      mensaje: '',
      mostrarMsj: false,
      formEditInst: {
        id: '',
        nombre: '',
        correo: '',
        direccion: '',
        telefono: '',
        ciudad: '',
      },
    };
  },
  components: {
    alert: Alert,
  },
  methods: {
    todoInstituciones() {
      const path = 'http://localhost:5000/instituciones';
      axios.get(path)
        .then((res) => {
          this.instituciones = res.data['instituciones'];
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    agregarInstitucion(payload) {
      const path = 'http://localhost:5000/instituciones';
      axios.post(path, payload)
        .then(() => {
          this.todoInstituciones();
          this.mensaje = 'Institución añadida!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.log(error);
          this.todoInstituciones();
        });
    },
    editInst(institucion) {
      this.formEditInst = institucion;
    },
    actualizarInstitucion(payload, idInstitucion) {
      const path = `http://localhost:5000/instituciones/${idInstitucion}`;
      axios.put(path, payload)
        .then(() => {
          this.todoInstituciones();
          this.mensaje = 'Institución actualizada!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoInstituciones();
        });
    },
    onSubmitUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditInst.hide();
      const payload = {
        _id: this.formEditInst._id,
        nombre: this.formEditInst.nombre,
        correo: this.formEditInst.correo,
        direccion: this.formEditInst.direccion,
        telefono: this.formEditInst.telefono,
        ciudad: this.formEditInst.ciudad,
      };
      this.actualizarInstitucion(payload, this.formEditInst._id);
    },
    activate() {
      setTimeout(() => this.mostrarMsj = false, 1500);
    },
    initForm() {
      this.formCrearInst.nombre = '';
      this.formCrearInst.correo = '';
      this.formCrearInst.direccion = '';
      this.formCrearInst.telefono = '';
      this.formCrearInst.ciudad = '';
      this.formEditInst._id = '';
      this.formEditInst.nombre = '';
      this.formEditInst.correo = '';
      this.formEditInst.direccion = '';
      this.formEditInst.telefono = '';
      this.formEditInst.ciudad = '';
    },
    onSubmit(evt) {
      evt.preventDefault();
      this.$refs.modalCrearInst.hide();
      const payload = {
        nombre: this.formCrearInst.nombre,
        correo: this.formCrearInst.correo,
        direccion: this.formCrearInst.direccion,
        telefono: this.formCrearInst.telefono,
        ciudad: this.formCrearInst.ciudad,
      };
      this.agregarInstitucion(payload);
      this.initForm();
    },
    onReset(evt) {
      evt.preventDefault();
      this.$refs.modalCrearInst.hide();
      this.initForm();
    },
    onResetUpdate(evt) {
      evt.preventDefault();
      this.$refs.modalEditInst.hide();
      this.initForm();
      this.todoInstituciones(); // why?
    },
    eliminarInstitucion(idInstitucion) {
      const path = `http://localhost:5000/instituciones/${idInstitucion}`;
      axios.delete(path)
        .then(() => {
          this.todoInstituciones();
          this.mensaje = 'Institución eliminada!';
          this.mostrarMsj = true;
          this.activate();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.todoInstituciones();
        });
    },
    OnEliminarInst(institucion) {
      this.eliminarInstitucion(institucion._id);
    },
  },
  created() {
    this.todoInstituciones();
  },
};